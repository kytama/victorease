# Victorease - Victory Futsal Web App for Admin Dashboard using Laravel 8
### Final Project Rizky Pratama for Mercu Buana University 



## Requirement
- [Docker Desktop](https://www.docker.com/products/docker-desktop)
- [Laravel 8](https://laravel.com/docs/8.x/installation)
- [Composer](https://getcomposer.org/)


## Installation
```
# Clone the repository from GitHub and open the directory:
git clone https

# cd into your project directory
cd victorease

# Run command sail up to build the docker container for your application
./vendor/bin/sail up
